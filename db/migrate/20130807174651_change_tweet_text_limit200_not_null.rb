class ChangeTweetTextLimit200NotNull < ActiveRecord::Migration
  def change
    change_column(:tweets, :text, :string, {null: false, limit: 200})
  end
end
