class CreateAtUsers < ActiveRecord::Migration
  def change
    create_table :at_users do |t|
      t.integer :user_id
      t.integer :tweet_id

      t.timestamps
    end
    add_index :at_users, :user_id
    add_index :at_users, :tweet_id
    add_index :at_users, [:user_id, :tweet_id], unique: true
  end
end

