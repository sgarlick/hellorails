class CreateFollowings < ActiveRecord::Migration
  def change
    create_table :followings do |t|
      t.integer :followed_users_id
      t.integer :follower_users_id

      t.timestamps
    end
    add_index :followings, :follower_users_id
    add_index :followings, :followed_users_id
    add_index :followings, [:follower_users_id, :followed_users_id], unique: true
  end
end
