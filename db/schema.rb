# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20130808212149) do

  create_table "at_users", force: true do |t|
    t.integer  "user_id"
    t.integer  "tweet_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "at_users", ["tweet_id"], name: "index_at_users_on_tweet_id", using: :btree
  add_index "at_users", ["user_id", "tweet_id"], name: "index_at_users_on_user_id_and_tweet_id", unique: true, using: :btree
  add_index "at_users", ["user_id"], name: "index_at_users_on_user_id", using: :btree

  create_table "followings", force: true do |t|
    t.integer  "followed_users_id"
    t.integer  "follower_users_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "followings", ["followed_users_id"], name: "index_followings_on_followed_users_id", using: :btree
  add_index "followings", ["follower_users_id", "followed_users_id"], name: "index_followings_on_follower_users_id_and_followed_users_id", unique: true, using: :btree
  add_index "followings", ["follower_users_id"], name: "index_followings_on_follower_users_id", using: :btree

  create_table "tweets", force: true do |t|
    t.string   "text",       limit: 200, null: false
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tweets", ["user_id"], name: "index_tweets_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "username"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
