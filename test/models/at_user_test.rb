require 'test_helper'

class AtUserTest < ActiveSupport::TestCase
  test "user defined" do
    assert AtUser.method_defined? :user
  end

  test "tweet defined" do
    assert AtUser.method_defined? :tweet
  end

  test "user_id validation" do
    assert !AtUser.new{{:tweet_id => 1}}.save
  end

  test "tweet_id validation" do
    assert !AtUser.new({:user_id => 1}).save
  end
end
