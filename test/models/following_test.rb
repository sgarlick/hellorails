require 'test_helper'

class FollowingTest < ActiveSupport::TestCase
  test "follower_users exist" do
    assert Following.method_defined? :follower_users
  end

  test "followed_users exist" do
    assert Following.method_defined? :followed_users
  end

  test "no followed_users_id" do
    assert !Following.new({:follower_users_id => 1}).save
  end

  test "no follower_users_id" do
    assert !Following.new({:followed_users_id => 1}).save
  end

  test "user is following" do
    user = users(:user1)
    user.follow!(users(:user3))
    assert user.following?(users(:user3))
  end

  test "user is no longer following" do
    user = users(:user1)
    user.unfollow!(users(:user2))
    assert !user.following?(users(:user2))
  end
end