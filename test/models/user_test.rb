require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "follower_users defined" do
    assert User.method_defined? :follower_users
  end


  test "followed_users defined" do
    assert User.method_defined? :followed_users
  end

  test "at_user defined" do
    assert User.method_defined? :at_users
  end

  test "tweets defined" do
    assert User.method_defined? :tweets
  end

  test "followings defined" do
    assert User.method_defined? :followings
  end

  test "followerings defined" do
    assert User.method_defined? :followerings
  end

  test "get tweets" do
    assert users(:user1).tweets.size == 2
  end

  test "get followers" do
    assert users(:user1).follower_users.size == 1
    assert users(:user1).followerings.size == 1
  end


  test "get followed" do
    assert users(:user1).followed_users.size == 1
    assert users(:user1).followings.size == 1
  end

end
