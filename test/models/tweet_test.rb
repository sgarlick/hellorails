require 'test_helper'

class TweetTest < ActiveSupport::TestCase
  test "no user for tweet" do
    tweet = Tweet.new
    assert !tweet.save
  end

  test "no blank tweets" do
    tweet = users(:user1).tweets.create(:text => "")
    assert !tweet.save
  end

  test "tweet belongs to user" do
    tweet = Tweet.find(1)
    assert tweet.user == users(:user1)
  end

  test "tweet at user" do
    tweet = Tweet.find(1)
    assert tweet.at_users.size == 1
  end

  test "tweet too long" do
    tweet = users(:user1).tweets.create(:text => "d"*201)
    assert !tweet.save
  end

  test "user defined" do
    assert Tweet.method_defined? :user
  end


  test "at_user defined" do
    assert Tweet.method_defined? :at_users
  end
end
