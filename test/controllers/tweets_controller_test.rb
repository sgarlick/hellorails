require 'test_helper'

class TweetsControllerTest < ActionController::TestCase

  test "throw not found" do
    sign_in users(:user1)
    assert_raises(ActiveRecord::RecordNotFound) {
      get :show, { :id => 8}
    }
  end

end