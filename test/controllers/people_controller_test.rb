require 'test_helper'

class PeopleControllerTest < ActionController::TestCase
  test "defined index" do
    assert PeopleController.method_defined? :index
  end
  test "defined show" do
    assert PeopleController.method_defined? :show
  end
  test "defined follow" do
    assert PeopleController.method_defined? :follow
  end

  test "defined unfollow" do
    assert PeopleController.method_defined? :unfollow
  end

  test "show person" do
    sign_in users(:user1)
    get :show, {:id=> :user1}
    assert_response :success
    assert assigns(:person)[:user] == users(:user1)
  end

  test "index people" do
    sign_in users(:user1)
    get :index
    assert_response :success
    assert assigns(:users).size == 3
  end
end