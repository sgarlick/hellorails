require 'test_helper'

class WelcomeControllerTest < ActionController::TestCase
  test "do redirect" do
    sign_in users(:user1)
    get :index
    assert_response :redirect
  end
end
