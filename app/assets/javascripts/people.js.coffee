$(document).on('ajax:success', '#follow', (e, data) ->
  $('#follow-button').hide()
  $('#unfollow-button').show()
)

$(document).on('ajax:error', '#follow', (e, data) ->
    alert('Error following')
)

$(document).on('ajax:success', '#unfollow', (e, data) ->
  $('#follow-button').show()
  $('#unfollow-button').hide()
)

$(document).on('ajax:error', '#unfollow', (e, data) ->
  alert('Error unfollowing')
)