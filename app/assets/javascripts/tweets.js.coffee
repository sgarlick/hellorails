$(document).on('ajax:success', 'form#new_tweet', (e, data) ->
  alert('Successful Tweet: ' + data.text)
  $('#tweet_text').val('')
)

$(document).on('ajax:error', 'form#new_tweet', (e, data) ->
  alert data.responseJSON.text
)

$(document).on('ajax:success', 'form.delete-tweet', (e, data) ->
  $(e.target.parentNode).remove()
)

$(document).on('ajax:error', 'form.delete-tweet', (e, data) ->
  alert 'error deleting'
)