class Tweet < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :text
  validates_length_of :text, :in => 1..200
  validates_presence_of :user

  has_many :at_users, foreign_key: :tweet_id, dependent: :destroy
  has_many :to_users, through: :at_users, source: :user
end
