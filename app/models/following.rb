class Following < ActiveRecord::Base

  belongs_to :follower_users, class_name: :User
  belongs_to :followed_users, class_name: :User
  validates :follower_users_id, presence: true
  validates :followed_users_id, presence: true
end
