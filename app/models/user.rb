class User < ActiveRecord::Base
  has_many :tweets, foreign_key: :user_id, dependent: :destroy
  has_many :followed_users, through: :followings, source: :follower_users
  has_many :follower_users, through: :followerings, source: :followed_users
  has_many :followings, foreign_key: :followed_users_id, dependent: :destroy
  has_many :followerings, foreign_key: :follower_users_id, class_name: :Following, dependent: :destroy

  has_many :at_users, foreign_key: :user_id, dependent: :destroy
  has_many :tweets_at_user, through: :at_users, source: :tweet

  def follow!(other_user)
    followings.create!(follower_users_id: other_user.id)
  end

  def following?(other_user)
    followings.find_by(follower_users_id: other_user.id)
  end

  def unfollow!(other_user)
     followings.find_by(follower_users_id: other_user.id).destroy!
  end

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessor :login

  validates :username,
            :uniqueness => {
                :case_sensitive => false
            }
  validates :email,
            :uniqueness => {
                :case_sensitive => false
            }

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions).first
    end
  end

end
