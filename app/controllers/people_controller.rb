class PeopleController < ApplicationController
  def show
    user = User.find_by_username(params[:id])
    tweets = user.tweets
    followed = user.followed_users
    tweets_at_users = user.tweets_at_user
    follower = user.follower_users

    @person = {:user => user, :tweets => tweets, :tweets_at_user => tweets_at_users, :following => followed, :followers => follower}
    respond_to do |format|
      format.xml { render :xml => @person}
      format.json { render :json => @person}
      format.html { render :view }
    end
  end

  def index
    @users = User.all
    respond_to do |format|
      format.xml { render :xml => @users}
      format.html { render :all }
      format.json { render :json => @users}
    end
  end

  #user will be following the user associated with :handle after this call, it doesnt the about their following state
  def follow
    user = User.find_by_username(params[:id])
    if !current_user.following?(user)
      current_user.follow!(user)
    end
    respond_to do |format|
      format.json { render :json => true, :status => :ok }
      format.xml { render :xml => true, :status => :ok }
      format.html { redirect_to person_url(params[:id])}
    end
  end

  #user will no long be following the user associated with :handle, it doesnt the about their following state
  def unfollow
    user = User.find_by_username(params[:id])
    if current_user.following?(user)
      current_user.unfollow!(user)
    end
    respond_to do |format|
      format.json { render :json => true, :status => :ok }
      format.xml { render :xml => true, :status => :ok }
      format.html { redirect_to person_url(params[:id])}
    end
  end
end
