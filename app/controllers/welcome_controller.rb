class WelcomeController < ApplicationController
  def index
    redirect_to person_url(current_user.username)
  end
end
