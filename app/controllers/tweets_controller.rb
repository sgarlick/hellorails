class TweetsController < ApplicationController
  def show
    @tweet = Tweet.find(params[:id])
    @to_users = @tweet.to_users
    respond_to do |format|
      format.xml { render :xml => {:tweet => @tweet, :to_users => @to_users } }
      format.json { render :json => {:tweet => @tweet, :to_users => @to_users } }
      format.html { render :view }
    end
  end

  def index
    @tweets = Tweet.all
    respond_to do |format|
      format.xml { render :xml => @tweets }
      format.json { render :json => @tweets }
      format.html { render :all }
    end
  end


  def destroy
    tweet = Tweet.find(params[:id])
    if current_user == tweet.user
      tweet.destroy!
      respond_to do |format|
        format.json { render :json => true, :status => :ok }
        format.xml { render :xml => true, :status => :ok }
        format.html { redirect_to root_url }
      end

    else
      head :unauthorized
    end

  end

  def create
    @tweet = current_user.tweets.create(text: params[:tweet][:text])
    if @tweet.valid?
      at_users = @tweet.text.scan(/@([a-z0-9_]+)/i)
      #think im missing something here.  why does this each fill u with a single entry array?
      at_users.each {|u|
        user = User.find_by_username(params[:id])
        @tweet.at_users.create({:user_id => user.id})
      }
      respond_to do |format|
        format.json { render :json => @tweet }
        format.xml { render :xml => @tweet }
        format.html { redirect_to root_url }
      end
    else
      respond_to do |format|
        format.json { render :json => @tweet.errors, :status => :bad_request }
        format.xml { render :xml => @tweet.errors, :status => :bad_request }
        format.html { redirect_to root_url, :flash => { :alert => @tweet.errors[:text] } }
      end
    end
  end

end
